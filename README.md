# Roommate Finder

[![pipeline_status](https://gitlab.com/ezio_99/roommate-finder/badges/master/pipeline.svg)](https://gitlab.com/ezio_99/roommate-finder/badges/master/pipeline.svg)
[![coverage](https://gitlab.com/ezio_99/roommate-finder/badges/master/coverage.svg?job=test)](https://gitlab.com/ezio_99/roommate-finder/badges/master/coverage.svg?job=test)
[![Python version](https://img.shields.io/badge/python-3.10-blue.svg)](https://www.python.org/downloads/release/python-3100/)
[![License: GPL v3](https://img.shields.io/badge/License-GPLv3-blue.svg)](https://www.gnu.org/licenses/gpl-3.0)

[![SonarCloud](https://sonarcloud.io/images/project_badges/sonarcloud-orange.svg)](https://sonarcloud.io/summary/new_code?id=iu-sqr-course_roommate-finder)

## About

Web application which aims to help people to find roommates.

## Development

### Init local dev env

1. Install required software:
    * [python3.10](https://www.python.org/downloads/)
    * [poetry](https://python-poetry.org/docs/#installation)
    * [direnv](https://direnv.net/docs/installation.html)
    * [make](https://www.gnu.org/software/make/#download)
    * [docker](https://docs.docker.com/engine/install/)
    * [docker-compose](https://docs.docker.com/compose/install/)

2. Create `.envrc` with your values based on `.envrc.example`

3. Allow envs

       direnv allow

4. Install pre-commit

       pip install pre-commit
       pre-commit install

5. Install dependencies

       poetry install

### Run application

1. Create containers

       make env

2. Create database

       make db

3. Apply migrations

       make migrate

4. Create admin user

       make admin

5. Run application

       make run

### Access to DB

You can access DB using local [pgadmin](http://localhost:5050/).
For DB host use `db`.

### Run tests

#### Locally

    make tests

#### Inside docker

Environment variables:

1. `UI_TESTS_UPDATE_SCREENSHOTS` - if set to `True`
   then screenshots for UI tests will be updated to current browser state

Command:

      make tests_docker

#### Load tests

Command:

       make load_test
