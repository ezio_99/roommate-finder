import factory

from core.models import Favorite
from factories.core.profile import ProfileFactory


class FavoriteFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Favorite

    user_profile = factory.SubFactory(ProfileFactory)
    liked_profile = factory.SubFactory(ProfileFactory)
