import factory

from core.models import Profile
from core.models.characteristic import Characteristic
from factories.core.user import UserFactory


class CharacteristicFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Characteristic

    name = factory.Faker("word")


class ProfileFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Profile

    user = factory.SubFactory(UserFactory)
    first_name = factory.Faker("first_name")
    last_name = factory.Faker("last_name")
    avatar = factory.django.ImageField(color="blue")

    @factory.post_generation
    def characteristics(self, create, extracted, **kwargs):
        if not create:
            return

        if extracted:
            for c in extracted:
                self.characteristics.add(c)  # type: ignore

    @factory.post_generation
    def preferences(self, create, extracted, **kwargs):
        if not create:
            return

        if extracted:
            for p in extracted:
                self.preferences.add(p)  # type: ignore
