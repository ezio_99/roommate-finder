import factory
from faker import Faker

from core.models import User

faker = Faker()


class UserFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = User

    first_name = factory.LazyFunction(lambda: faker.name())
    last_name = factory.LazyFunction(lambda: faker.name())
    email = factory.LazyFunction(lambda: faker.email())

    @classmethod
    def _create(cls, model_class, *args, **kwargs):
        user = model_class(*args, **kwargs)
        user.set_password(kwargs.get("password", "password"))
        user.save()
        return user
