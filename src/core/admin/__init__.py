from django.contrib import admin

from core import models
from core.admin.user import UserAdmin

admin.site.register(models.User, UserAdmin)
admin.site.register(models.Profile)
admin.site.register(models.Favorite)
admin.site.register(models.Characteristic)
