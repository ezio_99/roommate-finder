from typing import Collection, Iterable

from django.db import IntegrityError, models
from django.db.models import Count

from core.models.profile import Profile


class Favorite(models.Model):
    user_profile = models.ForeignKey(
        Profile, on_delete=models.CASCADE, related_name="user_profile"
    )
    liked_profile = models.ForeignKey(
        Profile, on_delete=models.CASCADE, related_name="liked_profile"
    )

    class Meta:
        constraints = [
            models.UniqueConstraint(
                fields=["user_profile", "liked_profile"],
                name="unique_liked_profile",
            )
        ]


class FavoriteManager:
    @staticmethod
    def get_favorite(user_profile: Profile, liked_profile: Profile):
        try:
            favorite = Favorite.objects.get(
                user_profile=user_profile, liked_profile=liked_profile
            )
            return favorite
        except Favorite.DoesNotExist:
            return None

    @staticmethod
    def save(user_profile: Profile, liked_profile: Profile):
        try:
            favorite = Favorite(user_profile=user_profile, liked_profile=liked_profile)
            favorite.save()
        except IntegrityError:
            return False
        return True

    @staticmethod
    def remove(favorite: Favorite):
        favorite.delete()

    @staticmethod
    def get_all(user_profile: Profile):
        return Favorite.objects.filter(user_profile=user_profile).all()

    @staticmethod
    def preprocess_profiles_relatively_to(
        my_profile: Profile, profiles: Iterable[Profile]
    ):
        return [
            {
                "id": p.id,
                "user": p.user,
                "first_name": p.first_name,
                "last_name": p.last_name,
                "characteristics": p.characteristics.all(),
                "preferences": p.preferences.all(),
                "avatar": p.avatar,
                "liked": p.id
                in FavoriteManager.get_all(my_profile).values_list(
                    "liked_profile", flat=True
                ),
                "contact": p.user.email,
            }
            for p in profiles
            if p.id != my_profile.id
        ]

    @staticmethod
    def get_suggestions(user_profile: Profile) -> Collection[Profile]:
        user_preferences = set(user_profile.preferences.all())
        suggestions = (
            Profile.objects.exclude(id=user_profile.id)
            .annotate(
                common_count=Count(
                    "characteristics__id",
                    filter=models.Q(characteristics__in=user_preferences),
                )
            )
            .filter(common_count__gte=len(user_preferences) * 0.6)
        )
        return suggestions

    @staticmethod
    def get_matches(profile: Profile):
        liked_profiles = Favorite.objects.filter(user_profile=profile).values_list(
            "liked_profile", flat=True
        )
        liked_by_profiles = Favorite.objects.filter(liked_profile=profile).values_list(
            "user_profile", flat=True
        )
        match_profiles = Profile.objects.filter(id__in=liked_profiles).filter(
            id__in=liked_by_profiles
        )
        return match_profiles.all()
