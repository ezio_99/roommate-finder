import hashlib
import io
from typing import Any

import requests
from django.conf import settings
from django.core.files import File
from django.db import models

from core.models.characteristic import Characteristic
from core.models.user import User


class Profile(models.Model):
    id = models.AutoField(primary_key=True)
    user = models.ForeignKey(User, on_delete=models.CASCADE)

    first_name = models.CharField(max_length=30)
    last_name = models.CharField(max_length=30)

    avatar = models.ImageField(default=None, null=True, upload_to="avatars")

    characteristics = models.ManyToManyField(
        Characteristic, related_name="profiles_with_characteristic"
    )
    preferences = models.ManyToManyField(
        Characteristic, related_name="profiles_with_preference"
    )

    def __str__(self):
        return f"{self.first_name} {self.last_name}"

    class Meta:
        constraints = [
            models.UniqueConstraint(
                fields=["id", "user"],
                name="unique_user",
            )
        ]


class ProfileManager:
    @staticmethod
    def get_profile_by_id(id):
        try:
            p = Profile.objects.get(id=id)
            return p
        except Profile.DoesNotExist:
            return None

    @staticmethod
    def get_profile(user) -> Profile | None:
        try:
            p = Profile.objects.get(user=user)
            return p
        except Profile.DoesNotExist:
            return None

    @staticmethod
    def get_profile_fields(user) -> dict[str, Any] | None:
        profile = ProfileManager.get_profile(user)
        if profile is None:
            return None
        fields = {
            "first_name": profile.first_name,
            "last_name": profile.last_name,
            "avatar": profile.avatar,
            "characteristics": profile.characteristics.all(),
            "preferences": profile.preferences.all(),
        }
        return fields

    @staticmethod
    def save(
        user,
        first_name: str,
        last_name: str,
        avatar,
        characteristics: list[Characteristic],
        preferences: list[Characteristic],
    ):
        if not first_name or not last_name:
            raise ValueError("first and last names must be non empty")
        profile = ProfileManager.get_profile(user)
        if profile is None:
            profile = Profile(user=user)
            profile.save()
        profile.first_name = first_name
        profile.last_name = last_name
        profile.characteristics.clear()
        profile.characteristics.add(*characteristics)
        profile.preferences.clear()
        profile.preferences.add(*preferences)
        if (
            profile.avatar is None
            or profile.avatar.name is None
            or len(profile.avatar.name) == 0
        ):
            profile.avatar = avatar
        elif avatar and avatar.read() != profile.avatar.read():
            profile.avatar = avatar
        profile.save()

    @staticmethod
    def gen_avatar(profile: Profile | None = None, full_name: str | None = None):
        if profile is None and full_name is None:
            return None
        name = (
            f"{profile.first_name}+{profile.last_name}"
            if full_name is None and profile is not None
            else full_name
        )
        url = f"{settings.AVATAR_SERVICE}?name={name}&size=300"
        response = requests.get(url, timeout=5)
        if response.status_code == 200:
            return File(
                file=io.BytesIO(response.content),
                name=hashlib.md5(response.content, usedforsecurity=False).hexdigest(),
            )
        return None

    @staticmethod
    def delete_profile(profile: Profile | None):
        if profile is None:
            return
        profile.delete()

    @staticmethod
    def search_profiles(conditions: dict[str, Any]):
        """Search for profiles based on the specified criteria."""
        first_name = conditions.get("first_name")
        last_name = conditions.get("last_name")
        characteristics = conditions.get("characteristics")

        query = models.Q()
        if first_name:
            query &= models.Q(first_name__exact=first_name)
        if last_name:
            query &= models.Q(last_name__exact=last_name)
        if characteristics:
            for characteristic in characteristics:
                query &= models.Q(characteristics__name__in=[characteristic])
                num_characteristics = len(characteristics)
                return (
                    Profile.objects.annotate(
                        num_matched=models.Count("characteristics", filter=query),
                    )
                    .filter(characteristics__name__in=characteristics)
                    .filter(num_matched=num_characteristics)
                )

        return Profile.objects.filter(query).all()
