from django.contrib.auth.base_user import BaseUserManager
from django.contrib.auth.models import AbstractUser
from django.db import models
from django.utils.translation import gettext_lazy as _
from django_prometheus.models import ExportModelOperationsMixin


class UserManager(BaseUserManager):
    def create_user(
        self,
        email,
        password=None,
        is_admin=False,
        is_staff=False,
        is_active=True,
        avatar=None,
        **kwargs
    ):
        if not email:
            raise ValueError("User must have an email")
        if not password:
            raise ValueError("User must have a password")

        user = self.model(email=self.normalize_email(email), **kwargs)
        user.set_password(password)
        user.is_superuser = is_admin
        user.is_staff = is_staff
        user.is_active = is_active
        user.avatar = avatar
        user.save(using=self._db)
        return user

    def create_superuser(self, email, password=None):
        if not email:
            raise ValueError("User must have an email")
        if not password:
            raise ValueError("User must have a password")

        user = self.model(email=self.normalize_email(email))
        user.set_password(password)
        user.is_superuser = True
        user.is_staff = True
        user.is_active = True
        user.save(using=self._db)
        return user


class User(ExportModelOperationsMixin("core_user"), AbstractUser):
    username = None
    groups = None
    user_permissions = None
    email = models.EmailField(_("email address"), unique=True)

    USERNAME_FIELD = "email"
    REQUIRED_FIELDS = []

    objects = UserManager()

    is_data_processing_signed = models.BooleanField(
        _("is data processing agreement signed"), null=False, default=False, blank=False
    )

    def __str__(self):
        return self.email
