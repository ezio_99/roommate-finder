from django.contrib.auth.decorators import login_required
from django.http import HttpRequest
from django.shortcuts import redirect, render
from django.views.decorators.http import require_http_methods

from core.forms.profile.edit import EditForm
from core.models.profile import ProfileManager


@login_required
@require_http_methods(["GET", "POST"])
def handler(request: HttpRequest):
    user = request.user
    if request.method == "POST":
        form = EditForm(request.POST, request.FILES)
        if form.is_valid():
            cd = form.cleaned_data
            avatar = cd["avatar"]
            if cd["generate_avatar"]:
                avatar = ProfileManager.gen_avatar(
                    full_name=f'{cd["first_name"]}+{cd["last_name"]}'
                )
            ProfileManager.save(
                user,
                cd["first_name"],
                cd["last_name"],
                avatar,
                cd["characteristics"],
                cd["preferences"],
            )
            return redirect("profile")
        return render(request, "core/profile/edit.html", {"form": form})
    profile = ProfileManager.get_profile_fields(user)
    if profile is None:
        form = EditForm()
    else:
        form = EditForm(initial=profile)
    return render(request, "core/profile/edit.html", {"form": form})
