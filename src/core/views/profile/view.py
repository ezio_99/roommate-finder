from django.contrib.auth.decorators import login_required
from django.http import HttpRequest
from django.shortcuts import redirect, render

from core.models.profile import ProfileManager


@login_required
def handler(request: HttpRequest):
    user = request.user
    profile = ProfileManager.get_profile_fields(user)
    if profile is None:
        return redirect("edit_profile")
    return render(request, "core/profile/view.html", {"profile": profile})
