from django.contrib.auth.decorators import login_required
from django.http import HttpRequest
from django.shortcuts import redirect

from core.models.profile import ProfileManager


@login_required
def handler(request: HttpRequest):
    user = request.user
    profile = ProfileManager.get_profile(user)
    ProfileManager.delete_profile(profile)
    return redirect("edit_profile")
