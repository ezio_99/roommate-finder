from django.contrib.auth.decorators import login_required
from django.shortcuts import redirect, render
from django.views.decorators.http import require_http_methods

from core.forms.profile.search import SearchForm
from core.models.favorite import FavoriteManager
from core.models.profile import ProfileManager


@login_required
@require_http_methods(["GET", "POST"])
def handler(request):
    user = request.user
    my_profile = ProfileManager.get_profile(user)
    if my_profile is None:
        return redirect("edit_profile")

    if request.method == "POST":
        form = SearchForm(request.POST)
        if form.is_valid():
            conditions = {
                "characteristics": [
                    c.name for c in form.cleaned_data["characteristics"]
                ],
            }
            profiles = ProfileManager.search_profiles(conditions)
            profiles = FavoriteManager.preprocess_profiles_relatively_to(
                my_profile, profiles
            )
            return render(
                request, "core/profile/search_results.html", {"profiles": profiles}
            )
    else:
        form = SearchForm()

    return render(request, "core/profile/search.html", {"form": form})
