from typing import Optional

import django.contrib.auth as auth
from django.shortcuts import redirect, render
from django.views.decorators.http import require_http_methods

from core.forms.auth import LoginForm
from core.models import User


def authenticate(request, email, password) -> Optional[User]:
    user = auth.authenticate(request, email=email, password=password)
    if user is None or (not user.is_active):
        return
    return user


def login(request, user):
    return auth.login(request, user)


@require_http_methods(["GET", "POST"])
def handler(request):
    if request.user.is_authenticated:
        return redirect("main")
    if request.method == "POST":
        form = LoginForm(request.POST)
        if form.is_valid():
            cd = form.cleaned_data
            user = authenticate(request, **cd)
            if user is None:
                form.add_error("", form.get_invalid_login_error())
                return render(request, "core/auth/login.html", {"form": form})
            login(request, user)
            return redirect("main")
    else:
        form = LoginForm()

    return render(request, "core/auth/login.html", {"form": form})
