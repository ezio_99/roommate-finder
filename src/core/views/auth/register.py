from django.shortcuts import redirect, render
from django.views.decorators.http import require_http_methods

from core.forms.auth import RegisterForm


@require_http_methods(["GET", "POST"])
def handler(request):
    if request.user.is_authenticated:
        return redirect("main")
    if request.method == "POST":
        form = RegisterForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("main")
        return render(request, "core/auth/register.html", {"form": form})
    else:
        form = RegisterForm()
    return render(request, "core/auth/register.html", {"form": form})
