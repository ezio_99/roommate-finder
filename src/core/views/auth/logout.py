import django.contrib.auth as auth
from django.shortcuts import redirect
from django.views.decorators.http import require_http_methods


def logout(request):
    auth.logout(request)


@require_http_methods(["GET"])
def handler(request):
    if request.user.is_authenticated:
        logout(request)
    return redirect("main")
