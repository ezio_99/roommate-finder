from django.shortcuts import render
from django.views.decorators.http import require_http_methods


@require_http_methods(["GET"])
def handler(request):
    return render(request, "./core/info/agreement.html")
