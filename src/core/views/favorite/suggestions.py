from django.contrib.auth.decorators import login_required
from django.http import HttpRequest
from django.shortcuts import redirect, render
from django.views.decorators.http import require_http_methods

from core.models.favorite import FavoriteManager
from core.models.profile import ProfileManager


@login_required
@require_http_methods(["GET"])
def handler(request: HttpRequest):
    user = request.user
    profile = ProfileManager.get_profile(user)
    if profile is None:
        return redirect("edit_profile")
    suggestions = FavoriteManager.get_suggestions(profile)
    suggestions = FavoriteManager.preprocess_profiles_relatively_to(
        profile, suggestions
    )
    return render(
        request,
        "core/profile/search_results.html",
        {"profiles": suggestions, "title": "Suggestions"},
    )
