from django.contrib.auth.decorators import login_required
from django.http import HttpRequest
from django.shortcuts import redirect, render
from django.views.decorators.http import require_http_methods

from core.models.favorite import FavoriteManager
from core.models.profile import ProfileManager


@login_required
@require_http_methods(["GET"])
def handler(request: HttpRequest):
    user = request.user
    profile = ProfileManager.get_profile(user)
    if profile is None:
        return redirect("edit_profile")
    liked_profiles = list(
        map(lambda f: f.liked_profile, FavoriteManager.get_all(profile))
    )
    liked_profiles = FavoriteManager.preprocess_profiles_relatively_to(
        profile, liked_profiles
    )
    return render(
        request,
        "core/profile/search_results.html",
        {"profiles": liked_profiles, "title": "Liked Profiles"},
    )
