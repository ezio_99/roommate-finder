import json

from django.contrib.auth.decorators import login_required
from django.http import (
    HttpRequest,
    HttpResponseBadRequest,
    HttpResponseNotFound,
    JsonResponse,
)
from django.views.decorators.http import require_http_methods

from core.models.favorite import FavoriteManager
from core.models.profile import ProfileManager


@login_required
@require_http_methods(["POST"])
def handler(request: HttpRequest):
    user = request.user
    try:
        body_data = json.loads(request.body)
    except ValueError:
        return HttpResponseBadRequest("Not JSON object")

    if body_data.get("profile_id") is None or body_data.get("action") is None:
        return HttpResponseBadRequest("Invalid JSON fields")

    liked_profile = ProfileManager.get_profile_by_id(body_data["profile_id"])
    if liked_profile is None:
        return HttpResponseNotFound("Profile not found")

    profile = ProfileManager.get_profile(user)
    if profile is None or profile == liked_profile:
        return HttpResponseBadRequest("Can not like own profile")

    if body_data["action"] == "add":
        FavoriteManager.save(profile, liked_profile)
        return JsonResponse({"status": "success"})

    if body_data["action"] == "remove":
        favorite = FavoriteManager.get_favorite(profile, liked_profile)
        if favorite is None:
            return HttpResponseNotFound()
        FavoriteManager.remove(favorite)
        return JsonResponse({"status": "success"})

    return HttpResponseBadRequest()
