from django.urls import path

import core.views.auth.agreement
import core.views.auth.login
import core.views.auth.logout
import core.views.auth.register
import core.views.favorite.add_or_remove
import core.views.favorite.matches
import core.views.favorite.suggestions
import core.views.favorite.view
import core.views.main
import core.views.profile.delete
import core.views.profile.edit
import core.views.profile.search
import core.views.profile.view

urlpatterns = [
    path("auth/login", core.views.auth.login.handler, name="login"),
    path("auth/register", core.views.auth.register.handler, name="register"),
    path("auth/logout", core.views.auth.logout.handler, name="logout"),
    path("profile", core.views.profile.view.handler, name="profile"),
    path("edit_profile", core.views.profile.edit.handler, name="edit_profile"),
    path("delete_profile", core.views.profile.delete.handler, name="delete_profile"),
    path("search_profiles", core.views.profile.search.handler, name="search_profiles"),
    path(
        "info/processing-personal-data",
        core.views.auth.agreement.handler,
        name="agreement",
    ),
    path(
        "favorite/like_profile",
        core.views.favorite.add_or_remove.handler,
        name="like",
    ),
    path(
        "favorite/profiles",
        core.views.favorite.view.handler,
        name="liked_profiles",
    ),
    path(
        "favorite/suggestions",
        core.views.favorite.suggestions.handler,
        name="suggestions",
    ),
    path("favorite/matches", core.views.favorite.matches.handler, name="matches"),
    path("", core.views.main.handler, name="main"),
]
