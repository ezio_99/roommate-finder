# Generated by Django 4.1.7 on 2023-04-10 18:23

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0002_profile'),
    ]

    operations = [
        migrations.AddConstraint(
            model_name='profile',
            constraint=models.UniqueConstraint(fields=('id', 'user'), name='unique_user'),
        ),
    ]
