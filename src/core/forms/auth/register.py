from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.utils.safestring import mark_safe

from core.models import User


class RegisterForm(UserCreationForm):
    is_data_processing_signed = forms.BooleanField(
        required=True,
        label=mark_safe(
            'I have read the <a href="/info/processing-personal-data">'
            "terms and conditions"
            "</a> and agree to the processing of my personal data"
        ),  # nosec
    )

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields["email"].widget.attrs.pop("autofocus", None)

    class Meta:
        model = User
        fields = ["email", "password1", "password2", "is_data_processing_signed"]
