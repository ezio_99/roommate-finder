from django import forms
from django.contrib.auth.forms import AuthenticationForm
from django.core.exceptions import ValidationError
from django.utils.translation import gettext_lazy as _

from core.models import User


class LoginForm(forms.Form):
    email = forms.EmailField(required=True, label="Email address")
    password = AuthenticationForm().fields["password"]

    class Meta:
        model = User
        fields = ["email", "password"]

    error_messages = {
        "invalid_login": _(
            "Please enter a correct email and password. Note that both "
            "fields may be case-sensitive."
        ),
    }

    def get_invalid_login_error(self):
        return ValidationError(
            self.error_messages["invalid_login"],
            code="invalid_login",
        )
