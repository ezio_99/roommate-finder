from django import forms

from core.models.characteristic import Characteristic
from core.models.profile import Profile


class EditForm(forms.Form):
    first_name = forms.CharField(max_length=30, min_length=2)
    last_name = forms.CharField(max_length=30, min_length=2)

    characteristics = forms.ModelMultipleChoiceField(
        queryset=Characteristic.objects.all()
    )
    preferences = forms.ModelMultipleChoiceField(queryset=Characteristic.objects.all())

    avatar = forms.ImageField(required=False)

    generate_avatar = forms.BooleanField(required=False)

    class Meta:
        model = Profile
        fields = ["first_name", "last_name", "preferences", "characteristics", "avatar"]
        widgets = {
            "characteristics": forms.CheckboxSelectMultiple,
            "preferences": forms.CheckboxSelectMultiple,
        }
