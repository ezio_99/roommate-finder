from django import forms

from core.models.characteristic import Characteristic


class SearchForm(forms.Form):
    characteristics = forms.ModelMultipleChoiceField(
        queryset=Characteristic.objects.all()
    )

    class Meta:
        widgets = {
            "characteristics": forms.CheckboxSelectMultiple,
        }
