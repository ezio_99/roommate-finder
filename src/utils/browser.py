from urllib.parse import urljoin

from playwright.sync_api import Page, Response


def go_to_page(
    page: Page, endpoint: str, base_url: str = "http://127.0.0.1:9999"
) -> Response | None:
    return page.goto(urljoin(base_url, endpoint))
