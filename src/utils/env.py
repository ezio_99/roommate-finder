import os


class EnvNotFound(Exception):
    pass


def get_env_or_fail(name: str) -> str:
    env = os.environ.get(name)
    if env is None:
        raise EnvNotFound(f"Env {name} not found")
    return env
