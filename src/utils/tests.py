import os
from typing import Literal

import faker
from django.contrib.sessions.middleware import SessionMiddleware
from django.test.client import RequestFactory
from playwright.sync_api import Page


def assert_screenshot(page: Page, screenshot_path: str) -> None:
    t: Literal["png", "jpeg"] = screenshot_path.split(".")[-1]  # type: ignore
    assert t in ("png", "jpeg")
    screenshot = page.screenshot(type=t)
    assert screenshot_path.endswith(f".{t}")
    if os.environ.get("UI_TESTS_UPDATE_SCREENSHOTS", "False").title() == "True":
        with open(screenshot_path, "wb") as f:
            f.write(screenshot)
    with open(screenshot_path, "rb") as f:
        from_disk = f.read()
        if from_disk != screenshot:
            with open(screenshot_path + f".failed.{t}", "wb") as nf:
                nf.write(screenshot)
        assert from_disk == screenshot


def ui_test(test_fn):
    if os.environ.get("ENABLE_UI_TESTS", "False").title() == "True":
        return test_fn
    else:

        def noop(*args, **kwargs):
            """Always pass"""
            pass

        return noop


def make_request(method, endpoint, *args, **kwargs):
    rf = RequestFactory()
    req = getattr(rf, method)(endpoint, *args, **kwargs)
    middleware = SessionMiddleware(req)
    middleware.process_request(req)
    req.session.save()
    return req


faker = faker.Faker()


def fake_email():
    return faker.email()


def fake_password():
    return faker.password() + "1#Aa"


def get_csrf(client, url):
    get_response = client.get(url)
    return get_response.context["csrf_token"]
