import os

from utils.tests import ui_test


def test_ui_test():
    @ui_test
    def test_fn():
        return "fn"

    os.environ["ENABLE_UI_TESTS"] = "True"
    assert test_fn() == "fn"

    os.environ.pop("ENABLE_UI_TESTS")
    assert test_fn() is None

    os.environ["ENABLE_UI_TESTS"] = "False"
    assert test_fn() is None
