import os

from utils.env import EnvNotFound, get_env_or_fail


def test_get_env_or_fail():
    os.environ.setdefault("ENV", "test")
    assert get_env_or_fail("ENV") == "test"
    os.environ.pop("ENV")

    try:
        get_env_or_fail("ENV")
    except Exception as e:
        assert isinstance(e, EnvNotFound)
        assert str(e) == "Env ENV not found"
