import pytest

from core.views.auth.login import authenticate, login
from core.views.auth.logout import logout
from factories.core.user import UserFactory
from utils.tests import make_request


@pytest.mark.django_db
def test_logout():
    user = UserFactory()
    req = make_request(
        "post", "/auth/login", {"email": user.email, "password": "password"}
    )
    user = authenticate(req, user.email, "password")
    login(req, user)
    logout(req)
    assert req.session.get("_auth_user_id") is None
    assert req.session.get("_auth_user_backend") is None
