import pytest

from core.views.auth.login import authenticate, login
from factories.core.user import UserFactory
from tests.consts import login_endpoint
from utils.tests import make_request


@pytest.mark.django_db
def test_authenticate_success():
    user = UserFactory()
    req = make_request(
        "post", login_endpoint, {"email": user.email, "password": "password"}
    )
    res = authenticate(req, user.email, "password")
    assert res == user


@pytest.mark.django_db
def test_authenticate_failure():
    req = make_request(
        "post", login_endpoint, {"email": "asd@asd.asd", "password": "password"}
    )
    res = authenticate(req, "asd@asd@asd", "password")
    assert res is None


@pytest.mark.django_db
def test_login():
    user = UserFactory()
    req = make_request(
        "post", login_endpoint, {"email": user.email, "password": "password"}
    )
    user = authenticate(req, user.email, "password")
    login(req, user)
    assert req.session["_auth_user_id"] == str(user.id)
    assert (
        req.session["_auth_user_backend"] == "django.contrib.auth.backends.ModelBackend"
    )
