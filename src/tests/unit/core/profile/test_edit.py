import pytest
from django.test import Client

from core.models.profile import Profile
from factories.core.profile import CharacteristicFactory, ProfileFactory
from factories.core.user import UserFactory
from tests.consts import edit_profile_endpoint


@pytest.mark.django_db
def test_edit_none_profile():
    user = UserFactory()
    client = Client()
    client.force_login(user)  # type: ignore
    response = client.get(edit_profile_endpoint)
    assert response.status_code == 200
    assert not response.context["form"].is_bound


@pytest.mark.django_db
def test_edit_existing_profile():
    user = UserFactory()
    profile = ProfileFactory.create(user=user)
    client = Client()
    client.force_login(user)  # type: ignore
    response = client.get(edit_profile_endpoint)
    assert response.status_code == 200
    assert response.context["form"].initial["first_name"] == profile.first_name
    assert response.context["form"].initial["last_name"] == profile.last_name
    assert set(response.context["form"].initial["preferences"]) == set(
        profile.preferences.all().values_list("name", flat=True)
    )
    assert set(response.context["form"].initial["characteristics"]) == set(
        profile.characteristics.all().values_list("name", flat=True)
    )
    assert response.context["form"].initial["avatar"] == profile.avatar


@pytest.mark.django_db
def test_post_valid_form():
    user = UserFactory()
    profile = ProfileFactory.create(user=user)
    client = Client()
    client.force_login(user)  # type: ignore
    characteristic = CharacteristicFactory(name="A")
    form_data = {
        "first_name": profile.first_name,
        "last_name": profile.last_name,
        "preferences": [str(characteristic.id)],  # type: ignore
        "characteristics": [str(characteristic.id)],  # type: ignore
        "generate_avatar": False,
    }
    response = client.post(edit_profile_endpoint, form_data, follow=True)
    assert response.redirect_chain == [("/profile", 302)]
    updated_profile = Profile.objects.get(user=user)
    assert updated_profile.first_name == profile.first_name
    assert updated_profile.last_name == profile.last_name
    assert set(profile.characteristics.all().values_list("name", flat=True)) == set(
        ["A"]
    )
    assert set(updated_profile.preferences.all().values_list("name", flat=True)) == set(
        ["A"]
    )


@pytest.mark.django_db
def test_post_invalid_form():
    user = UserFactory()
    client = Client()
    client.force_login(user)  # type: ignore
    _ = ProfileFactory(user=user)
    response = client.post(
        edit_profile_endpoint,
        {
            "first_name": "",
            "last_name": "",
            "avatar": "",
            "generate_avatar": False,
            "characteristics": ["invalid"],
            "preferences": ["invalid"],
        },
        follow=True,
    )
    assert response.status_code == 200
    assert "form" in response.context
    assert response.context["form"].errors
    assert Profile.objects.filter(user=user).exists()
