# type:ignore
import hashlib
from unittest.mock import patch

import pytest
from django.conf import settings
from django.core.files import File
from requests import Response

from core.models.profile import Profile, ProfileManager
from factories.core.profile import CharacteristicFactory, ProfileFactory
from factories.core.user import UserFactory


@pytest.mark.django_db
def test_get_profile():
    user = UserFactory()
    profile = ProfileFactory(user=user)
    assert ProfileManager.get_profile(user) == profile
    Profile.objects.filter(user=user).delete()
    assert ProfileManager.get_profile(user) is None


@pytest.mark.django_db
def test_get_profile_by_id():
    user = UserFactory()
    profile = ProfileFactory(user=user)
    profile_id = profile.id
    assert ProfileManager.get_profile_by_id(profile_id) == profile
    Profile.objects.filter(id=profile_id).delete()
    assert ProfileManager.get_profile_by_id(profile_id) is None


@pytest.mark.django_db
def test_get_profile_fields():
    user = UserFactory()
    profile = ProfileFactory(user=user)
    fields = ProfileManager.get_profile_fields(user)
    assert fields["first_name"] == profile.first_name  # type:ignore
    assert fields["last_name"] == profile.last_name  # type:ignore
    assert fields["avatar"].read() == profile.avatar.read()  # type:ignore
    assert set(fields["characteristics"]) == set(  # type:ignore
        profile.characteristics.all()
    )
    assert set(fields["preferences"]) == set(  # type:ignore
        profile.preferences.all()
    )
    Profile.objects.filter(user=user).delete()
    assert ProfileManager.get_profile_fields(user) is None


@pytest.mark.django_db
def test_generate_avatar():
    user = UserFactory()
    profile = ProfileFactory(user=user)
    url = f"{settings.AVATAR_SERVICE}?name={profile.first_name}+{profile.last_name}&size=300"
    expected_file_name = hashlib.md5("dummy".encode()).hexdigest()
    with patch("requests.get") as mock_request:
        mock_request.return_value = Response()
        mock_request.return_value.status_code = 200
        mock_request.return_value._content = "dummy".encode()
        avatar = ProfileManager.gen_avatar(profile=profile)
        assert isinstance(avatar, File)
        assert avatar.name == expected_file_name
        mock_request.assert_called_once_with(url, timeout=5)


@pytest.mark.django_db
def test_delete_profile():
    user = UserFactory()
    profile = ProfileFactory(user=user)
    ProfileManager.delete_profile(profile=profile)
    assert ProfileManager.get_profile(user) is None
    ProfileManager.delete_profile(profile=None)
    assert ProfileManager.get_profile(user) is None


@pytest.mark.django_db
def test_save_invalid_profile():
    user = UserFactory()
    try:
        ProfileManager.save(
            user=user,
            first_name="",
            last_name="",
            avatar=None,
            characteristics=[],
            preferences=[],
        )
    except ValueError:
        assert True
    else:
        assert False


@pytest.mark.django_db
def test_save_existing_profile():
    user = UserFactory()
    profile = ProfileFactory(user=user)
    profile.characteristics.set(  # type:ignore
        [CharacteristicFactory(name="C")]
    )

    profile.preferences.set(
        [
            CharacteristicFactory(name="A"),
            CharacteristicFactory(name="B"),
            CharacteristicFactory(name="C"),
        ]
    )

    ProfileManager.save(
        user=user,
        first_name="abc",
        last_name="abc",
        avatar=None,
        characteristics=[
            CharacteristicFactory(name="C"),
            CharacteristicFactory(name="D"),
        ],
        preferences=[
            CharacteristicFactory(name="A"),
            CharacteristicFactory(name="B"),
            CharacteristicFactory(name="C"),
            CharacteristicFactory(name="D"),
        ],
    )

    profile = Profile.objects.get(user=user)
    assert profile.first_name == "abc"
    assert profile.last_name == "abc"
    assert set(profile.characteristics.all().values_list("name", flat=True)) == set(
        ["C", "D"]
    )
    assert set(profile.preferences.all().values_list("name", flat=True)) == set(
        ["A", "B", "C", "D"]
    )


@pytest.mark.django_db
def test_save_non_existing_profile():
    user = UserFactory()

    ProfileManager.save(
        user=user,
        first_name="abc",
        last_name="abc",
        avatar=None,
        characteristics=[
            CharacteristicFactory(name="C"),
        ],
        preferences=[
            CharacteristicFactory(name="A"),
            CharacteristicFactory(name="B"),
            CharacteristicFactory(name="C"),
        ],
    )
    profile = Profile.objects.get(user=user)
    assert profile.first_name == "abc"
    assert profile.last_name == "abc"
    assert set(profile.characteristics.all().values_list("name", flat=True)) == set(
        ["C"]
    )
    assert set(profile.preferences.all().values_list("name", flat=True)) == set(
        ["A", "B", "C"]
    )


@pytest.mark.django_db
def test_search_profiles():
    user1 = UserFactory()
    user2 = UserFactory()
    user3 = UserFactory()
    _ = ProfileFactory(user=user1)
    _ = ProfileFactory(user=user2)
    _ = ProfileFactory(user=user3)
    a = CharacteristicFactory(name="A")
    b = CharacteristicFactory(name="B")
    c = CharacteristicFactory(name="C")
    ProfileManager.save(
        user=user1,
        first_name="abc",
        last_name="abc",
        avatar=None,
        characteristics=[a, b, c],
        preferences=[c],
    )
    ProfileManager.save(
        user=user2,
        first_name="abc2",
        last_name="abc2",
        avatar=None,
        characteristics=[a],
        preferences=[c],
    )
    ProfileManager.save(
        user=user3,
        first_name="abc3",
        last_name="abc3",
        avatar=None,
        characteristics=[a, b],
        preferences=[c],
    )

    res = ProfileManager.search_profiles(conditions={"characteristics": ["A"]})
    assert res.count() == 3

    res = ProfileManager.search_profiles(conditions={"characteristics": ["A", "B"]})
    assert res.count() == 2

    res = ProfileManager.search_profiles(
        conditions={"characteristics": ["A", "B", "C"]}
    )
    assert res.count() == 1
    assert res.first().first_name == "abc"  # type:ignore

    res = ProfileManager.search_profiles(
        conditions={"characteristics": ["A"], "first_name": "abc"}
    )
    assert res.count() == 1
