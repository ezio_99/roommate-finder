import pytest
from django.test import Client

from core.models.profile import Profile
from factories.core.profile import ProfileFactory
from factories.core.user import UserFactory


@pytest.mark.django_db
def test_delete_existing_profile():
    user = UserFactory()
    profile = ProfileFactory.create(user=user)
    client = Client()
    client.force_login(user)  # type: ignore
    response = client.post("/delete_profile", follow=True)
    assert response.redirect_chain == [("/edit_profile", 302)]
    assert not Profile.objects.filter(id=profile.id).exists()


@pytest.mark.django_db
def test_delete_non_existing_profile():
    user = UserFactory()
    client = Client()
    client.force_login(user)  # type: ignore
    response = client.post("/delete_profile", follow=True)
    assert response.redirect_chain == [("/edit_profile", 302)]
    assert not Profile.objects.filter(user=user).exists()
