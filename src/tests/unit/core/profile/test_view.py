import pytest
from django.test import Client

from factories.core.profile import ProfileFactory
from factories.core.user import UserFactory


@pytest.mark.django_db
def test_get_none_profile():
    user = UserFactory()
    client = Client()
    client.force_login(user)  # type: ignore
    response = client.get("/profile", follow=True)
    assert response.redirect_chain == [("/edit_profile", 302)]


@pytest.mark.django_db
def test_get_existing_profile():
    user = UserFactory()
    profile = ProfileFactory.create(user=user)
    client = Client()
    client.force_login(user)  # type: ignore
    response = client.get("/profile")
    assert response.status_code == 200
    assert response.context["profile"]["first_name"] == profile.first_name
    assert response.context["profile"]["last_name"] == profile.last_name
