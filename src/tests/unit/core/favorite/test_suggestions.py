import pytest
from django.test import Client

from factories.core.profile import CharacteristicFactory, ProfileFactory
from factories.core.user import UserFactory


@pytest.mark.django_db
def test_suggestions():
    client = Client()
    user1 = UserFactory()
    user2 = UserFactory()
    c1, c2, c3, c4, c5, c6 = [CharacteristicFactory(name=str(i)) for i in range(6)]
    ProfileFactory(user=user1, preferences=[c1, c2, c3, c4, c5])
    ProfileFactory(user=user2, characteristics=[c1, c2, c3, c4, c5, c6])
    client.force_login(user1)  # type: ignore
    response = client.get("/favorite/suggestions")
    assert response.status_code == 200
    assert len(response.context["profiles"]) == 1


@pytest.mark.django_db
def test_none_profile():
    client = Client()
    user = UserFactory()
    client.force_login(user)  # type: ignore
    response = client.get("/favorite/suggestions", follow=True)
    assert response.redirect_chain == [("/edit_profile", 302)]
