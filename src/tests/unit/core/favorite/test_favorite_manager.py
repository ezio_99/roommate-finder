import pytest

from core.models.favorite import Favorite, FavoriteManager
from factories.core.favorite import FavoriteFactory
from factories.core.profile import CharacteristicFactory, ProfileFactory
from factories.core.user import UserFactory


@pytest.fixture
def my_profile():
    user = UserFactory()
    profile = ProfileFactory(user=user)
    return profile


@pytest.fixture
def liked_profile():
    user = UserFactory()
    profile = ProfileFactory(user=user)
    return profile


@pytest.fixture
def favorite(my_profile, liked_profile):
    favorite = FavoriteFactory(user_profile=my_profile, liked_profile=liked_profile)
    return favorite


@pytest.fixture
def match(my_profile, liked_profile):
    FavoriteFactory(user_profile=my_profile, liked_profile=liked_profile)
    FavoriteFactory(user_profile=liked_profile, liked_profile=my_profile)


@pytest.mark.django_db
def test_get_favorite(favorite):
    result = FavoriteManager.get_favorite(favorite.user_profile, favorite.liked_profile)
    assert result == favorite


@pytest.mark.django_db
def test_get_favorite_not_found(my_profile, liked_profile):
    result = FavoriteManager.get_favorite(my_profile, liked_profile)
    assert result is None


@pytest.mark.django_db
def test_save(my_profile, liked_profile):
    result = FavoriteManager.save(my_profile, liked_profile)
    assert result


@pytest.mark.django_db
def test_save_duplicate(my_profile, liked_profile, favorite):
    result = FavoriteManager.save(my_profile, liked_profile)
    assert not result


@pytest.mark.django_db
def test_remove(favorite):
    FavoriteManager.remove(favorite)
    assert not Favorite.objects.filter(
        user_profile=favorite.user_profile, liked_profile=favorite.liked_profile
    ).exists()


@pytest.mark.django_db
def test_get_all(my_profile, liked_profile, favorite):
    result = FavoriteManager.get_all(my_profile)
    assert result.count() == 1
    assert result[0] == favorite


@pytest.mark.django_db
def test_preprocess_possible_match_profiles(my_profile, liked_profile):
    profiles = [liked_profile]
    result = FavoriteManager.preprocess_profiles_relatively_to(my_profile, profiles)
    assert len(result) == 1
    assert result[0]["id"] == liked_profile.id
    assert result[0]["user"] == liked_profile.user
    assert result[0]["first_name"] == liked_profile.first_name
    assert result[0]["last_name"] == liked_profile.last_name
    assert list(result[0]["characteristics"]) == list(
        liked_profile.characteristics.all()
    )
    assert list(result[0]["preferences"]) == list(liked_profile.preferences.all())
    assert result[0]["avatar"] == liked_profile.avatar
    assert not result[0]["liked"]


@pytest.mark.django_db
def test_get_suggestions():
    c1, c2, c3, c4, c5, c6 = [CharacteristicFactory(name=str(i)) for i in range(6)]
    profile = ProfileFactory(preferences=[c1, c2, c3, c4, c5])
    other_profile_1 = ProfileFactory(characteristics=[c1, c2, c3, c4])
    other_profile_2 = ProfileFactory(characteristics=[c1])
    other_profile_3 = ProfileFactory(characteristics=[c1, c2, c3, c4, c5, c6])
    other_profile_4 = ProfileFactory(characteristics=[c1, c2, c3, c4, c5])
    suggestions = FavoriteManager.get_suggestions(profile)  # type: ignore
    assert len(suggestions) == 3
    assert other_profile_2 not in suggestions
    assert other_profile_1 in suggestions
    assert other_profile_3 in suggestions
    assert other_profile_4 in suggestions


@pytest.mark.django_db
def test_get_matches(my_profile, liked_profile, match):
    assert len(FavoriteManager.get_matches(my_profile)) == 1
    assert FavoriteManager.get_matches(liked_profile).first() == my_profile


@pytest.mark.django_db
def test_no_matches(my_profile, liked_profile, favorite):
    assert len(FavoriteManager.get_matches(my_profile)) == 0
    assert len(FavoriteManager.get_matches(liked_profile)) == 0
