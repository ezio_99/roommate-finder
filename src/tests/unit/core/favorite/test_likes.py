import pytest
from django.test import Client

from factories.core.profile import ProfileFactory
from factories.core.user import UserFactory
from tests.consts import json_content_type, like_profile_endpoint


@pytest.mark.django_db
def test_invalid_body():
    user = UserFactory()
    client = Client()
    client.force_login(user)  # type: ignore
    data = "invalid"
    response = client.post(like_profile_endpoint, data, content_type=json_content_type)
    assert response.status_code == 400
    assert response.content.decode() == "Not JSON object"


@pytest.mark.django_db
def test_invalid_body_fields():
    user = UserFactory()
    client = Client()
    client.force_login(user)  # type: ignore
    data = {"invalid": "invalid"}
    response = client.post(like_profile_endpoint, data, content_type=json_content_type)
    assert response.status_code == 400
    assert response.content.decode() == "Invalid JSON fields"


@pytest.mark.django_db
def test_liked_profile_is_none():
    client = Client()
    user = UserFactory()
    client.force_login(user)  # type: ignore
    data = {"profile_id": 1111, "action": "add"}
    response = client.post(like_profile_endpoint, data, content_type=json_content_type)
    assert response.status_code == 404
    assert response.content.decode() == "Profile not found"


@pytest.mark.django_db
def test_liked_profile_is_user_profile():
    client = Client()
    user = UserFactory()
    profile = ProfileFactory(user=user)
    client.force_login(user)  # type: ignore
    data = {"profile_id": profile.id, "action": "add"}
    response = client.post(like_profile_endpoint, data, content_type=json_content_type)
    assert response.status_code == 400
    assert response.content.decode() == "Can not like own profile"


@pytest.mark.django_db
def test_actions():
    client = Client()
    user1 = UserFactory()
    user2 = UserFactory()
    ProfileFactory(user=user1)
    profile2 = ProfileFactory(user=user2)
    client.force_login(user1)  # type: ignore
    data = {"profile_id": profile2.id, "action": "add"}
    response = client.post(like_profile_endpoint, data, content_type=json_content_type)
    assert response.status_code == 200

    data = {"profile_id": profile2.id, "action": "remove"}
    response = client.post(like_profile_endpoint, data, content_type=json_content_type)
    assert response.status_code == 200
