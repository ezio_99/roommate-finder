import pytest
from django.test import Client

from factories.core.favorite import FavoriteFactory
from factories.core.profile import ProfileFactory
from factories.core.user import UserFactory


@pytest.mark.django_db
def test_view():
    client = Client()
    user1 = UserFactory()
    user2 = UserFactory()
    profile1 = ProfileFactory(user=user1)
    profile2 = ProfileFactory(user=user2)
    FavoriteFactory(user_profile=profile1, liked_profile=profile2)
    client.force_login(user1)  # type: ignore
    response = client.get("/favorite/profiles")
    assert response.status_code == 200


@pytest.mark.django_db
def test_none_profile():
    client = Client()
    user = UserFactory()
    client.force_login(user)  # type: ignore
    response = client.get("/favorite/profiles", follow=True)
    assert response.redirect_chain == [("/edit_profile", 302)]
