import os
from typing import Callable, List, Tuple

from playwright.sync_api import Page

# ATTENTION ================================================================
# if some forms has autofocus, you need to cancel this autofocus
# before making a screenshot, because playwright do not wait for autofocus
# to happen and your UI test will be inconsistent and could fail sometimes.
# Alternatively you could unset autofocus from forms.
# Just click on text or something else and wait a couple of milliseconds or
# fill autofocused field with some text.

# ==========================================================================


def path(p: str) -> str:
    return os.path.join("src/tests/ui/screenshots", p)


# Simple UI tests config
# Format: (url, selector, path)
# url: URL to visit
# selector: CSS selector to wait for
# path: path to screenshot
simple_ui_tests_config: List[Tuple[str, str, str]] = [
    ("/admin", "text=Django administration", path("admin.jpeg")),
    ("/auth/login", "text=Log in", path("auth/login.jpeg")),
    ("/auth/register", "text=Registration", path("auth/register.jpeg")),
    ("/info/processing-personal-data", "text=Privacy Policy", path("agreement.jpeg")),
]


def click(selector: str):
    return lambda page: page.click(selector)


def click_and_wait(selector: str, timeout_ms: int = 100):
    def fn(page: Page):
        page.click(selector)
        page.wait_for_timeout(timeout_ms)

    return fn


# Advanced UI tests config
# Format: (url, selector, path, fn)
# url: URL to visit
# selector: CSS selector to wait for
# path: path to screenshot
# fn: function to run before waiting for selector
advanced_ui_tests_config: List[Tuple[str, str, str, Callable]] = [
    (
        "/auth/login",
        "text=Log in",
        path("auth/login_failed.jpeg"),
        click("button[type=submit]"),
    ),
]
