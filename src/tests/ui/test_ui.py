import pytest
from playwright.sync_api import Page

from tests.ui.config import advanced_ui_tests_config, simple_ui_tests_config
from utils.browser import go_to_page
from utils.tests import assert_screenshot, ui_test


@pytest.mark.parametrize("url, selector, path", simple_ui_tests_config)
@ui_test
def test_ui_simple(url, selector, path, page: Page):
    go_to_page(page, url)
    page.wait_for_selector(selector, timeout=3000)
    assert_screenshot(page, path)


@pytest.mark.parametrize("url, selector, path, fn", advanced_ui_tests_config)
@ui_test
def test_ui_advanced(url, selector, path, fn, page: Page):
    go_to_page(page, url)
    fn(page)
    page.wait_for_selector(selector, timeout=3000)
    assert_screenshot(page, path)
