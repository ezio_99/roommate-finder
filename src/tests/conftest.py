import pytest
from django.test import Client

DJANGO_SETTINGS_MODULE = "tests.settings"


@pytest.fixture
def example_fixture():
    return 1


@pytest.fixture(scope="session")
def browser_context_args(browser_context_args):
    return {
        **browser_context_args,
        "viewport": {
            "width": 1920,
            "height": 1080,
        },
    }


@pytest.fixture
def csrf_client():
    return Client(enforce_csrf_checks=True)
