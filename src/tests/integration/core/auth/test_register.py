import pytest

from core.models import User
from utils.tests import fake_email, fake_password, get_csrf


@pytest.mark.django_db
def test_register_success(client):
    password = fake_password()
    credentials = {
        "email": fake_email(),
        "password1": password,
        "password2": password,
        "is_data_processing_signed": True,
        "csrfmiddlewaretoken": get_csrf(client, "/auth/register"),
    }
    r = client.post("/auth/register", credentials)
    print(r.request)
    print(r.content)
    assert User.objects.filter(email=credentials["email"]).exists()


@pytest.mark.django_db
def test_register_fail_password_mismatch(client):
    password = fake_password()
    credentials = {
        "email": fake_email(),
        "password1": password,
        "password2": password + "1",
        "csrfmiddlewaretoken": get_csrf(client, "/auth/register"),
    }
    client.post("/auth/register", credentials)
    assert not User.objects.filter(email=credentials["email"]).exists()


@pytest.mark.django_db
def test_register_fail_no_csrf_token(client):
    password = fake_password()
    credentials = {
        "email": fake_email(),
        "password1": password,
        "password2": password + "1",
    }
    client.post("/auth/register", credentials)
    assert not User.objects.filter(email=credentials["email"]).exists()


@pytest.mark.django_db
def test_register_fail_invalid_email(client):
    password = fake_password()
    credentials = {
        "email": "asd",
        "password1": password,
        "password2": password,
        "csrfmiddlewaretoken": get_csrf(client, "/auth/register"),
    }
    client.post("/auth/register", credentials)
    assert not User.objects.filter(email=credentials["email"]).exists()
