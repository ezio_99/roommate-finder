import hashlib
import io
from unittest.mock import patch

import pytest
from django.core.files import File
from django.test import Client
from requests import Response

from core.models.profile import Profile
from factories.core.profile import CharacteristicFactory, ProfileFactory
from factories.core.user import UserFactory


@pytest.mark.django_db
def test_auto_generate_avatar():
    user = UserFactory()
    ProfileFactory(user=user)
    client = Client()
    client.force_login(user=user)  # type: ignore
    characteristic = CharacteristicFactory(name="A")
    form_data = {
        "first_name": "Test",
        "last_name": "User",
        "characteristics": [str(characteristic.id)],  # type: ignore
        "preferences": [str(characteristic.id)],  # type: ignore
        "generate_avatar": True,
    }
    with patch("requests.get") as mock_request:
        mock_request.return_value = Response()
        mock_request.return_value.status_code = 200
        mock_request.return_value._content = "dummy".encode()
        client.post("/edit_profile", form_data, follow=True)
        expected_avatar = File(
            file=io.BytesIO("dummy".encode()),
            name=hashlib.md5("dummy".encode(), usedforsecurity=False).hexdigest(),
        )
        profile = Profile.objects.get(user=user)
        assert profile.avatar.read() == expected_avatar.read()
