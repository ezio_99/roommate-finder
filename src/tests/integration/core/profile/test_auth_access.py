import pytest
from django.test import Client

from factories.core.profile import ProfileFactory
from factories.core.user import UserFactory
from tests.consts import edit_profile_endpoint


@pytest.mark.django_db
def test_auth_access_to_view():
    user = UserFactory()
    _ = ProfileFactory(user=user)
    client = Client()
    r = client.get("/profile", follow=True)
    assert r.redirect_chain == [("/auth/login?next=/profile", 302)]
    client.force_login(user=user)  # type: ignore
    r = client.get("/profile")
    assert r.status_code == 200


@pytest.mark.django_db
def test_auth_access_to_edit():
    user = UserFactory()
    _ = ProfileFactory(user=user)
    client = Client()
    r = client.get(edit_profile_endpoint, follow=True)
    assert r.redirect_chain == [("/auth/login?next=/edit_profile", 302)]
    client.force_login(user=user)  # type: ignore
    r = client.get(edit_profile_endpoint)
    assert r.status_code == 200


@pytest.mark.django_db
def test_auth_access_to_delete():
    user = UserFactory()
    _ = ProfileFactory(user=user)
    client = Client()
    r = client.get("/delete_profile", follow=True)
    assert r.redirect_chain == [("/auth/login?next=/delete_profile", 302)]
    client.force_login(user=user)  # type: ignore
    r = client.get("/delete_profile", follow=True)
    assert r.redirect_chain == [(edit_profile_endpoint, 302)]
