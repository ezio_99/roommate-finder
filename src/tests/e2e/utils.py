from playwright.sync_api import Page

from utils.browser import go_to_page
from utils.tests import fake_email, fake_password


def fill_register_form(page: Page, email, password1, password2, is_agree):
    if password2 is None:
        password2 = password1
    page.query_selector("input[name=email]").fill(email)
    page.query_selector("input[name=password1]").fill(password1)
    page.query_selector("input[name=password2]").fill(password2)
    if is_agree:
        page.query_selector("input[name=is_data_processing_signed]").click()


def fill_login_form(page: Page, email, password):
    page.query_selector("input[name=email]").fill(email)
    page.query_selector("input[name=password]").fill(password)


def fill_profile_form(page: Page, first_name, last_name, preference, characteristic):
    page.query_selector("input[name=first_name]").fill(first_name)
    page.query_selector("input[name=last_name]").fill(last_name)
    page.query_selector("select[name=characteristics]").select_option(
        characteristic, timeout=3000
    )
    page.query_selector("select[name=preferences]").select_option(
        preference, timeout=3000
    )


def click_submit_button(page: Page):
    page.query_selector("button[type=submit]").click()


def register(page: Page, email=None, password1=None, password2=None, is_agree=None):
    go_to_page(page, "/auth/register")
    page.wait_for_selector("text=Registration", timeout=3000)
    if email is None:
        email = fake_email()
    if password1 is None:
        password1 = fake_password()
    if password2 is None:
        password2 = password1
    if is_agree is None:
        is_agree = True
    fill_register_form(page, email, password1, password2, is_agree)
    click_submit_button(page)
    return email, password1


def login(page: Page, email, password):
    go_to_page(page, "/auth/login")
    page.wait_for_selector("text=Log in", timeout=3000)
    fill_login_form(page, email, password)
    click_submit_button(page)
