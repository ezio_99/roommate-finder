from playwright.sync_api import Page

from tests.e2e.utils import click_submit_button, fill_profile_form, login, register


def test_liked_profiles(page: Page):
    email, password = register(page)
    login(page, email, password)
    assert page.wait_for_selector("text=Edit profile")
    fill_profile_form(page, "first_name", "last_name", "A", "A")
    click_submit_button(page)
    page.click("text=Liked Profiles")
    assert page.wait_for_selector("text=Liked Profiles", timeout=3000)
