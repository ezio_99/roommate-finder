import pytest
from playwright.sync_api import Page

from tests.consts import (
    login_endpoint,
    login_selector,
    profile_edit_selector,
    register_endpoint,
)
from tests.e2e.utils import login, register
from utils.browser import go_to_page
from utils.tests import fake_email, fake_password


def test_mismatch_passwords(page: Page):
    password1 = fake_password()
    password2 = password1 + "1"
    register(page, fake_email(), password1, password2)
    assert page.wait_for_selector(
        "text=The two password fields didn’t match.", timeout=3000
    )
    assert page.url.endswith(register_endpoint)


def register_with_used_email(page: Page):
    email, password = fake_email(), fake_password()
    register(page, email, password)
    assert page.wait_for_selector(login_selector, timeout=3000)
    register(page, email, password)
    assert page.wait_for_selector(
        "text=User with this Email address already exists.", timeout=3000
    )
    assert page.url.endswith(register_endpoint)


@pytest.mark.parametrize(
    "email, password, text_selector",
    [
        (None, "asdasdasd", "This password is too common."),
        (None, "12345678", "This password is entirely numeric."),
        (
            "ivan.ivanov@example.com",
            "ivanivanov",
            "The password is too similar to the email address.",
        ),
        (
            None,
            "1",
            "This password is too short. It must contain at least 8 characters.",
        ),
    ],
)
def test_weak_password(page: Page, email, password, text_selector):
    register(page, email, password)
    assert page.wait_for_selector(f"text={text_selector}", timeout=3000)
    assert page.url.endswith(register_endpoint)


def test_register_success(page: Page):
    register(page)
    assert page.wait_for_selector(login_selector, timeout=3000)
    assert login_endpoint in page.url


def test_registered_redirect(page: Page):
    email, password = register(page)
    login(page, email, password)
    go_to_page(page, register_endpoint)
    assert page.wait_for_selector(profile_edit_selector, timeout=3000)


def test_login_link(page: Page):
    go_to_page(page, register_endpoint)
    page.click(login_selector)
    assert page.wait_for_selector(login_selector, timeout=3000)
    assert login_endpoint in page.url


def test_agreement_link(page: Page):
    go_to_page(page, register_endpoint)
    page.click("text=terms and conditions")
    assert page.wait_for_selector("text=Privacy Policy", timeout=3000)
    assert "/info/processing-personal-data" in page.url
