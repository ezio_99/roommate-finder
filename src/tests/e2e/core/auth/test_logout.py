from playwright.sync_api import Page

from tests.consts import login_endpoint, login_selector, profile_edit_selector
from tests.e2e.utils import login, register
from utils.browser import go_to_page


def test_access_main_success(page: Page):
    email, password = register(page)
    login(page, email, password)
    assert page.wait_for_selector(profile_edit_selector, timeout=3000)


def test_access_main_fail(page: Page):
    go_to_page(page, "")
    assert page.wait_for_selector(login_selector, timeout=3000)
    assert login_endpoint in page.url


def test_logout_success(page: Page):
    email, password = register(page)
    login(page, email, password)
    assert page.wait_for_selector(profile_edit_selector, timeout=3000)
    page.get_by_text("Log Out").click()
    assert page.wait_for_selector(login_selector, timeout=3000)
    assert login_endpoint in page.url
