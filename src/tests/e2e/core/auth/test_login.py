from playwright.sync_api import Page

from tests.consts import (
    login_endpoint,
    login_selector,
    profile_edit_selector,
    register_endpoint,
    register_selector,
)
from tests.e2e.utils import login, register
from utils.browser import go_to_page


def test_invalid_email(page: Page):
    email, password = "wrong_email", "password"
    login(page, email, password)
    assert page.wait_for_selector("text=Enter a valid email address.", timeout=3000)
    assert page.url.endswith(login_endpoint)


def test_wrong_credentials(page: Page):
    email, password = "wrong_email@example.com", "wrong_password"
    login(page, email, password)
    assert page.wait_for_selector(
        "text=Please enter a correct email and password. Note that both fields may be case-sensitive.",
        timeout=3000,
    )
    assert page.url.endswith(login_endpoint)


def test_logined_redirect(page: Page):
    email, password = register(page)
    assert page.wait_for_selector(login_selector, timeout=3000)
    assert login_endpoint in page.url
    login(page, email, password)
    assert page.wait_for_selector(profile_edit_selector, timeout=3000)
    go_to_page(page, login_endpoint)
    assert page.wait_for_selector(profile_edit_selector, timeout=3000)


def test_login_success(page: Page):
    email, password = register(page)
    assert page.wait_for_selector(login_selector, timeout=3000)
    assert login_endpoint in page.url
    login(page, email, password)
    assert page.wait_for_selector(profile_edit_selector, timeout=3000)


def test_register_link(page: Page):
    go_to_page(page, login_endpoint)
    page.click(register_selector)
    assert page.wait_for_selector(register_selector, timeout=3000)
    assert register_endpoint in page.url
