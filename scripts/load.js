import http from 'k6/http';
import { sleep } from 'k6';

throw Error("fill csrftoken and sessionid")

export const options = {
  vus: 150,
  duration: '15s',
};

export default function () {
  const csrftoken = ""
  const sessionid = ""
  http.get('http://127.0.0.1:8000/profile', {
    cookies: {
      csrftoken,
      sessionid,
    }
  });
  http.get('http://127.0.0.1:8000/favorite/suggestions', {
    cookies: {
      csrftoken,
      sessionid,
    }
  });
  sleep(1);
}
