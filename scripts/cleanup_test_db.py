import psycopg2

from utils.env import get_env_or_fail


def main():
    POSTGRES_DB = get_env_or_fail("PG_DB")
    POSTGRES_USER = get_env_or_fail("PG_USER")
    POSTGRES_PASSWORD = get_env_or_fail("PG_PASSWORD")
    POSTGRES_HOST = get_env_or_fail("PG_HOST")
    POSTGRES_PORT = get_env_or_fail("PG_PORT")

    conn = psycopg2.connect(
        database=POSTGRES_DB,
        user=POSTGRES_USER,
        password=POSTGRES_PASSWORD,
        host=POSTGRES_HOST,
        port=POSTGRES_PORT,
    )
    try:
        conn.autocommit = True
        cur = conn.cursor()
        try:
            cur.execute(f"DROP DATABASE IF EXISTS test_{POSTGRES_DB}")
            print(f"Database test_{POSTGRES_DB} dropped")
        finally:
            cur.close()
    finally:
        conn.close()


if __name__ == "__main__":
    main()
