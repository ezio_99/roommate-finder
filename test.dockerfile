FROM grommash99/python_pw:3.10

WORKDIR /app

ENV PYTHONUNBUFFERED 1

COPY pyproject.toml /app
COPY poetry.lock /app

RUN pip3 install --upgrade pip poetry==1.3.2 && \
    poetry config virtualenvs.create false && \
    poetry install

COPY src /app/src
COPY scripts /app/scripts

CMD poetry run python3 ./src/manage.py migrate && \
    PYTHONPATH=src poetry run python3 ./scripts/cleanup_test_db.py && \
    ./scripts/tests
