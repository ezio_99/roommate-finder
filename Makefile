run:
	poetry run python3 src/manage.py runserver

tests:
	./scripts/tests

tests_docker:
	docker compose -f docker-compose.test.yml up --build --abort-on-container-exit

env:
	docker compose up -d

migrate:
	poetry run python3 src/manage.py migrate

migrations:
	poetry run python3 src/manage.py makemigrations

stop_env:
	docker compose stop

down_env:
	docker compose down

db:
	docker exec -i roommate_finder_db psql -U postgres -c "DROP DATABASE IF EXISTS ${PG_DB}"
	docker exec -i roommate_finder_db psql -U postgres -c "CREATE DATABASE ${PG_DB}"
	docker exec -i roommate_finder_db psql -U postgres -c "DROP USER IF EXISTS ${PG_USER}"
	docker exec -i roommate_finder_db psql -U postgres -c "CREATE USER ${PG_USER} WITH ENCRYPTED PASSWORD '${PG_PASSWORD}'"
	docker exec -i roommate_finder_db psql -U postgres -c "GRANT ALL PRIVILEGES ON DATABASE ${PG_DB} TO ${PG_USER}"
	docker exec -i roommate_finder_db psql -U postgres -c "ALTER USER ${PG_USER} CREATEDB"

rm_db_volume:
	docker volume rm roommate_finder_postgres

admin:
	poetry run python3 src/manage.py createsuperuser --email ${ADMIN_EMAIL}

load_test:
	docker run --net host --rm -i grafana/k6 run - <scripts/load.js
